//
//  HomeViewController.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/26/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    enum HomeSectionType: Int {
        case Search
        case Artist
    }
    
    enum HomeCellType: Int {
        case Collection
    }
    
    var refreshControl: UIRefreshControl!
    var artists: [Artist] = []
    var nextPage: String? = nil
    var searchTF: UITextField = UITextField ()
    var lastSearchedText: String? = nil
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Kogi Test"
        self.loadTableView()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .Default
    }
    
    func loadTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(self.reloadArtists), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
    }
    
    func homeSectionList() -> [HomeSectionType] {
        if self.artists.count > 0 {
            var selectionList: [HomeSectionType] = [.Search]
            for  _ in self.artists {
                selectionList += [.Artist]
            }
            return selectionList
        }
        return [.Search]
    }
    
    func homeCellList(sectionType: HomeSectionType) -> [HomeCellType] {
        switch sectionType {
        case .Search:
            return []
        case .Artist:
            return [.Collection]
        }
    }
    
    // MARK: - Selectors
    
    func searchButtonTapped (sender: UIButton) {
        if !self.searchTF.hasText() {
            return
        }
        self.lastSearchedText = self.searchTF.text!
        let searchString = self.lastSearchedText!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        self.requestForArtists(searchString)
    }
    
    func artistNameButtonTapped (sender: UIButton) {
       self.showArtistDetailView(self.artists[sender.tag - 1])
    }
    
    // MARK: - Custom functions
    
    func requestForArtists (searchString: String) {
        AppManager.sharedManager.showHUD(self.view)
        KogiAPI.sharedAPI.searchArtists(searchString, successBlock: { (request) in
            self.refreshControl.endRefreshing()
            AppManager.sharedManager.hideHUD(self.view)
            if request.items.count > 0 {
                self.artists = request.items
                self.nextPage = request.next
                self.tableView.reloadData()
            } else {
                self.artists = []
                self.nextPage = nil
                self.tableView.reloadData()
                self.showAlertSingleOption(localizedStringForKey("Sorry"), message: localizedStringForKey("No_Results_Message"))
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            AppManager.sharedManager.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("Sorry"), message: localizedStringForKey("Error_Message"))
        }
    }
    
    func reloadArtists () {
        if let text = self.lastSearchedText {
            let searchString = text.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            self.requestForArtists(searchString)
            return
        }
        self.refreshControl.endRefreshing()
    }
    
    func getNextPage () {
        AppManager.sharedManager.showHUD(self.view)
        KogiAPI.sharedAPI.getArtistNextPage(self.nextPage!, successBlock: { (request) in
            AppManager.sharedManager.hideHUD(self.view)
            self.artists += request.items
            self.nextPage = request.next
            self.tableView.reloadData()
            }) { (error) in
                AppManager.sharedManager.hideHUD(self.view)
                self.showAlertSingleOption(localizedStringForKey("Sorry"), message: localizedStringForKey("Error_Message"))
        }
    }
    
    func showArtistDetailView (artist: Artist) {
        AppManager.sharedManager.showHUD(self.view)
        KogiAPI.sharedAPI.getArtistAlbums(artist.id, successBlock: { (request) in
            AppManager.sharedManager.hideHUD(self.view)
            artist.albums = request.items
            self.pushToView(ViewFactory.AppView.Detail) { (viewController) in
                if let vc = viewController as? DetailViewController {
                    vc.artist = artist
                    vc.nextPage = request.next
                }
            }
            }) { (error) in
                AppManager.sharedManager.hideHUD(self.view)
                self.showAlertSingleOption(localizedStringForKey("Sorry"), message: localizedStringForKey("Error_Message"))
        }
        
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.homeSectionList().count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        return self.artists[section - 1].images.count > 0 ? 1 : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:GridView = GridView(style: UITableViewCellStyle.Default, reuseIdentifier: GridView.preferredIdentifier())
        cell.addImagesToGrid(self.artists[indexPath.section - 1].images)
        if (indexPath.section - 1) == self.artists.count - 1 && self.nextPage != nil {
            self.getNextPage ()
        }
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let homeSection = self.homeSectionList()[section]
        switch homeSection {
        case .Search:
            let searchView = SearchHeader ()
            searchView.searchTF.text = self.lastSearchedText
            self.searchTF = searchView.searchTF
            searchView.searchBtn.addTarget(self, action: #selector(self.searchButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return searchView
        case .Artist:
            let artistView = SingleButtonHeader ()
            artistView.btn.setTitle(self.artists[section - 1].name, forState: UIControlState.Normal)
            artistView.btn.tag = section
            artistView.btn.addTarget(self, action: #selector(self.artistNameButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return artistView
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return GridView.preferredHeight()
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = self.homeSectionList()[section]
        switch section {
        case .Search:
            return SearchHeader.preferredHeight()
        case .Artist:
            return SingleButtonHeader.preferredHeight()
        }
    }
}
//
//  WebViewController.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    var webView = UIWebView ()
    var urlString: String = "https://open.spotify.com/album/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupWebView ()
    }
    
    func setupWebView () {
        
        let toolbar = UIToolbar ()
        toolbar.opaque = false
        toolbar.barTintColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.AppSecundaryColor)
        toolbar.tintColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.AppMainColor)
        let backButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.Rewind, target: self, action: #selector(self.backButtonTapped))
        let flexibleSpace = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
        let forwardButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.FastForward, target: self, action: #selector(self.forwardButtonTapped))
        let refreshButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: #selector(self.refreshButtonTapped))
        toolbar.setItems([backButton,flexibleSpace,refreshButton,flexibleSpace,forwardButton], animated: false)
        
        self.view.addSubview(self.webView)
        self.view.addSubview(toolbar)
        
        self.webView.snp_makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        toolbar.snp_makeConstraints { (make) in
            make.left.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        self.webView.loadRequest(NSURLRequest (URL: NSURL(string: urlString)!))
        self.webView.delegate = self
    }
    
    // MARK: - Actions
    
    func backButtonTapped() {
        self.webView.goBack()
    }
    
    func forwardButtonTapped() {
        self.webView.goForward()
    }
    
    func refreshButtonTapped() {
        self.webView.reload()
    }
}

// MARK: - WebView Delegate

extension WebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(webView: UIWebView) {
        AppManager.sharedManager.showHUD(self.view)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        AppManager.sharedManager.hideHUD(self.view)
    }
}

//
//  DetailViewController.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class DetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    enum DetailSectionType: Int {
        case Detail
        case Albums
    }
    
    enum DetailCellType: Int {
        case Followers
        case Popularity
        case Album
    }
    
    var artist: Artist = Artist ()
    var nextPage: String? = nil
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = artist.name
        self.loadTableView()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .Default
    }
    
    func loadTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func detailSectionList() -> [DetailSectionType] {
        return [.Detail, .Albums]
    }
    
    func detailCellList(sectionType: DetailSectionType) -> [DetailCellType] {
        switch sectionType {
        case .Detail:
            return [.Followers, .Popularity]
        case .Albums:
            var cellList: [DetailCellType] = []
            for _ in self.artist.albums {
                cellList += [.Album]
            }
            return cellList
        }
    }
    
    // MARK: - Custom Methods
    
    func getNextPage () {
        AppManager.sharedManager.showHUD(self.view)
        KogiAPI.sharedAPI.getAlbumsNextPage(self.nextPage!, successBlock: { (request) in
            AppManager.sharedManager.hideHUD(self.view)
            self.artist.albums += request.items
            self.nextPage = request.next
            self.tableView.reloadData()
        }) { (error) in
            AppManager.sharedManager.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("Sorry"), message: localizedStringForKey("Error_Message"))
        }
    }
    
    func showAlbumWebView (album: Album) {
        self.pushToView(ViewFactory.AppView.WebView) { (viewController) in
            if let vc = viewController as? WebViewController {
                vc.urlString += album.id
            }
        }
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.detailSectionList().count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.detailCellList(self.detailSectionList()[section]).count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellType:DetailCellType = self.detailCellList(self.detailSectionList()[indexPath.section])[indexPath.row]
        switch cellType {
        case .Followers:
            let cell:DoubleLabelCell = DoubleLabelCell(style: UITableViewCellStyle.Default, reuseIdentifier: DoubleLabelCell.preferredIdentifier())
            cell.mainLbl.text = localizedStringForKey("Followers")
            cell.secundaryLbl.text = "\(self.artist.followers.total)"
            return cell
        case .Popularity:
            let cell:DoubleLabelCell = DoubleLabelCell(style: UITableViewCellStyle.Default, reuseIdentifier: DoubleLabelCell.preferredIdentifier())
            cell.mainLbl.text = localizedStringForKey("Popularity")
            cell.secundaryLbl.text = "\(self.artist.popularity)"
            return cell
        case .Album:
            let cell:AlbumDetailCell = AlbumDetailCell(style: UITableViewCellStyle.Default, reuseIdentifier: AlbumDetailCell.preferredIdentifier())
            let album = self.artist.albums[indexPath.row]
            let imageURL = album.images[0].url
            cell.albumImageView.setImageWithURL(NSURL (string: imageURL)!)
            cell.titleLbl.text = album.name
            if album.markets.count > 5 {
                cell.marketsListLbl.text = "5+"
            } else {
                cell.marketsListLbl.text = album.markets.joinWithSeparator(", ")
            }
            if (indexPath.row) == self.artist.albums.count - 1 && self.nextPage != nil {
                self.getNextPage ()
            }
            return cell
        }
    }

    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let sectionType: DetailSectionType = self.detailSectionList()[indexPath.section]
        switch sectionType {
        case .Detail:
            return
        case .Albums:
            self.showAlbumWebView(self.artist.albums[indexPath.row])
            return
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionType: DetailSectionType = self.detailSectionList()[section]
        switch sectionType {
        case .Detail:
            let header = CenteredImageHeader ()
            if self.artist.images.count > 0 {
                header.centerImageView.setImageWithURL(NSURL(string: self.artist.images[0].url)!)
            } else {
                header.centerImageView.image = nil
            }
            return header
        case .Albums:
            let header = SingleButtonHeader ()
            header.btn.setTitle(localizedStringForKey("Albums"), forState: UIControlState.Normal)
            header.btn.tag = section
            return header
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cellType:DetailCellType = self.detailCellList(self.detailSectionList()[indexPath.section])[indexPath.row]
        switch cellType {
        case .Followers, .Popularity:
            return DoubleLabelCell.preferredHeight()
        case .Album:
            return AlbumDetailCell.preferredHeight()
        }
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionType: DetailSectionType = self.detailSectionList()[section]
        switch sectionType {
        case .Detail:
            return CenteredImageHeader.preferredHeight()
        case .Albums:
            return SingleButtonHeader.preferredHeight()
        }
    }
    
}

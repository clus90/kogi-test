//
//  KogiAPI.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import Mantle

class KogiAPI: NSObject {
    
    static let sharedAPI = KogiAPI()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    
    func requestWithRequest(request:KogiAPIRequest, pathArgs: [String]?, parseClass:AnyClass, completion:(response:AnyObject?) -> (), error: (error:AnyObject?) -> ()) {
        request.requestAPI(pathArgs, completion: { (responseObject) in
            var json: NSDictionary
            if (request.startNode != nil) {
                json = responseObject[request.startNode!] as!  NSDictionary
            } else {
                json = responseObject as! NSDictionary
            }
            do {
                let object:AnyObject = try MTLJSONAdapter.modelOfClass(parseClass, fromJSONDictionary: json as [NSObject : AnyObject])
                completion(response: object)
            } catch {
                print ("catch")
            }
        }) { (operation) in
            error (error: operation)
        }
    }
    
    func requestWithRequest(request:KogiAPIRequest, pathArgs: [String]?, parseClassArray:AnyClass, completion:(response:[AnyObject]?) -> (), error: (error:AnyObject?) -> ()) {
        request.requestAPI(pathArgs, completion: { (responseObject) in
            var jsonArray: [AnyObject]
            if (request.startNode != nil) {
                jsonArray = responseObject[request.startNode!] as!  [AnyObject]
            } else {
                jsonArray = responseObject as! [AnyObject]
            }
            var array:[AnyObject]
            do {
                array = try MTLJSONAdapter.modelsOfClass(parseClassArray, fromJSONArray: jsonArray)
            } catch {
                array = []
            }
            completion (response: array)
        }) { (operation) in
            error (error: operation)
        }
    }
    
    func requestWithRequest(request:KogiAPIRequest, pathArgs: [String]?, completion:(response:AnyObject?) -> (), error: (error:AnyObject?) -> ()) {
        request.requestAPI(pathArgs, completion: { (responseObject) in
            var response: NSDictionary
            if (request.startNode != nil) {
                response = responseObject[request.startNode!] as! NSDictionary
            } else {
                response = responseObject as! NSDictionary
            }
            completion (response: response)
        }) { (operation) in
            error (error: operation)
        }
    }
    
    func requestWithURL (request:KogiAPIRequest, urlString: String, parseClass:AnyClass, completion:(response:AnyObject?) -> (), error: (error:AnyObject?) -> ()) {
        request.requestAPI(path: urlString, completion: { (responseObject) in
            var json: NSDictionary
            if (request.startNode != nil) {
                json = responseObject[request.startNode!] as!  NSDictionary
            } else {
                json = responseObject as! NSDictionary
            }
            do {
                let object:AnyObject = try MTLJSONAdapter.modelOfClass(parseClass, fromJSONDictionary: json as [NSObject : AnyObject])
                completion(response: object)
            } catch {
                print ("catch")
            }
            }) { (operation) in
                error (error: operation)
        }
    }
}



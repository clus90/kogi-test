//
//  APIModules.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import CoreLocation

extension KogiAPI {
    
    func searchArtists(searchString: String, successBlock:(request: RequestArtist) -> (), errorBlock: (error: AnyObject?) -> ()) {
        let request = KogiAPIRequest (service: KogiAPIRequest.APIService.SearchArtists, parameters: nil, startNode: "artists")
        KogiAPI.sharedAPI.requestWithRequest(request, pathArgs: [searchString], parseClass: RequestArtist.self, completion: { (response) in
            if let response = response as? RequestArtist {
                successBlock (request: response)
            } else {
                errorBlock (error: nil)
            }
            }) { (error) in
                print ("Error: \(error)")
                errorBlock (error: error)
        }
    }
    
    func getArtistNextPage (urlString: String, successBlock:(request: RequestArtist) -> (), errorBlock: (error: AnyObject?) -> ()) {
        let request = KogiAPIRequest (service: KogiAPIRequest.APIService.SearchArtists, parameters: nil, startNode: "artists")
        KogiAPI.sharedAPI.requestWithURL(request, urlString: urlString, parseClass: RequestArtist.self, completion: { (response) in
            if let response = response as? RequestArtist {
                successBlock (request: response)
            } else {
                errorBlock (error: nil)
            }
            }) { (error) in
                print ("Error: \(error)")
                errorBlock (error: error)
        }
    }
    
    func getArtistAlbums (id: String, successBlock:(request: RequestAlbum) -> (), errorBlock: (error: AnyObject?) -> ()) {
        let request = KogiAPIRequest (service: KogiAPIRequest.APIService.GetAlbums, parameters: nil, startNode: nil)
        KogiAPI.sharedAPI.requestWithRequest(request, pathArgs: [id], parseClass: RequestAlbum.self, completion: { (response) in
            if let response = response as? RequestAlbum {
                successBlock (request: response)
            } else {
                errorBlock (error: nil)
            }
            }) { (error) in
                print ("Error: \(error)")
                errorBlock (error: error)
        }
    }
    
    func getAlbumsNextPage (urlString: String, successBlock:(request: RequestAlbum) -> (), errorBlock: (error: AnyObject?) -> ()) {
        let request = KogiAPIRequest (service: KogiAPIRequest.APIService.GetAlbums, parameters: nil, startNode: nil)
        KogiAPI.sharedAPI.requestWithURL(request, urlString: urlString, parseClass: RequestAlbum.self, completion: { (response) in
            if let response = response as? RequestAlbum {
                successBlock (request: response)
            } else {
                errorBlock (error: nil)
            }
        }) { (error) in
            print ("Error: \(error)")
            errorBlock (error: error)
        }
    }
}

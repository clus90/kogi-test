//
//  KogiAPIRequest.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import AFNetworking

class KogiAPIRequest: NSObject {
    
    let baseURL = "https://api.spotify.com/"
    
    enum APIError: ErrorType {
        case WrongParameters
    }
    
    enum APIService {
        case SearchArtists
        case GetAlbums
    }
    
    enum RequestMethod {
        case GET
    }
    
    var service: APIService
    var parameters:AnyObject?
    var startNode: String?
    var manager = AFHTTPRequestOperationManager ()
    
    init(service:APIService, parameters:AnyObject?, startNode: String?)  {
        self.service = service
        self.parameters = parameters
        self.startNode = startNode
        self.manager.requestSerializer = AFJSONRequestSerializer ()
        self.manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/html") as Set<NSObject>
    }
    
    func requestAPI (path path: String, completion:(responseObject: AnyObject) -> Void, error:(operation: AnyObject?) -> Void) {
        self.apiGET(path, success: { (responseObject) in
            completion (responseObject: responseObject)
            }, errorBlock: { (operation) in
                error (operation: operation)
        })
    }
    
    func requestAPI (pathArgs: [String]?, completion:(responseObject: AnyObject) -> Void, error:(operation: AnyObject?) -> Void) {
        let request: (path:String, method:RequestMethod) = self.path(pathArgs)
        switch request.method {
        case .GET:
            self.apiGET(request.path, success: { (responseObject) in
                completion (responseObject: responseObject)
                }, errorBlock: { (operation) in
                    error (operation: operation)
            })
            break
        }
    }
    
    private func path(pathArgs: [String]?) -> (String, RequestMethod) {
        var path:String = ""
        var requestMethod: RequestMethod = RequestMethod.GET
        do {
            switch self.service {
            case .SearchArtists:
                try self.checkParameters(pathArgs, expectedCount: 1)
                path = "v1/search?q=\(pathArgs![0])&type=artist"
                requestMethod = RequestMethod.GET
            case .GetAlbums:
                try self.checkParameters(pathArgs, expectedCount: 1)
                path = "v1/artists/\(pathArgs![0])/albums"
                requestMethod = RequestMethod.GET
            }
        } catch APIError.WrongParameters {
            print ("Error in parameters")
        } catch {
            print ("Unexpected error in APIService")
        }
        return (baseURL+path, requestMethod)
    }
    
    private func apiGET (path: String, success: (responseObject: AnyObject) -> Void, errorBlock: (operation: AnyObject?) -> Void) {
        self.manager.GET(path, parameters: self.parameters, success: { (operation, responseObject) in
            success (responseObject: responseObject)
        }) { (operation, error) in
            errorBlock (operation: operation?.responseObject)
        }
    }
    
    private func checkParameters (pathArgs: [String]?, expectedCount: Int) throws -> Void {
        if let pathArgs = pathArgs {
            if pathArgs.count != expectedCount {
                throw APIError.WrongParameters
            }
        }
    }
    
}


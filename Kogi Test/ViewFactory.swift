//
//  ViewFactory.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/26/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class ViewFactory: NSObject {
    
    enum AppView:Int {
        case Home
        case Detail
        case WebView
    }
    
    class func viewControllerForAppView(view:AppView) -> UIViewController {
        let viewController:UIViewController?
        
        switch view {
        case .Home:
            viewController = UINavigationController(rootViewController: HomeViewController())
        case .Detail:
            viewController = DetailViewController ()
        case .WebView:
            viewController = WebViewController ()
        }
        return viewController!
    }
    
    
}

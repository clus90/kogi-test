//
//  ArtistFollower.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class ArtistFollower: BaseModel {
    
    var href: String?
    var total: Int = 0
    
    override class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["href":"href",
                "total":"total"]
    }
    
    
}

//
//  Album.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import Mantle

class Album: BaseModel {
    
    var id: String = ""
    var markets: [String] = []
    var name: String = ""
    var href: String = ""
    var images: [Image] = []
    
    override class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["id":"id",
                "markets":"available_markets",
                "name":"name",
                "href":"href",
                "images":"images"]
    }
    
    class func imagesJSONTransformer () -> NSValueTransformer {
        return MTLJSONAdapter.arrayTransformerWithModelClass(Image.self)
    }
    
}
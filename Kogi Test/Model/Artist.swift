//
//  Artist.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import Mantle

class Artist: BaseModel {
    
    var id: String = ""
    var name: String = ""
    var href: String = ""
    var popularity: Int = 0
    var images: [Image] = []
    var followers: ArtistFollower = ArtistFollower ()
    var albums: [Album] = []
    
    override class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["id":"id",
                "name":"name",
                "href":"href",
                "images":"images",
                "popularity":"popularity",
                "followers":"followers"]
    }
    
    class func imagesJSONTransformer () -> NSValueTransformer {
        return MTLJSONAdapter.arrayTransformerWithModelClass(Image.self)
    }
    
    class func followersJSONTransformer () -> NSValueTransformer {
        return MTLJSONAdapter.dictionaryTransformerWithModelClass(ArtistFollower.self)
    }
}

//
//  RequestAlbum.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import Mantle

class RequestAlbum: BaseModel {
    
    var href: String = ""
    var next: String?
    var items: [Album] = []
    
    override class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["href":"href",
                "next":"next",
                "items":"items"]
    }
    
    class func itemsJSONTransformer () -> NSValueTransformer {
        return MTLJSONAdapter.arrayTransformerWithModelClass(Album.self)
    }
}

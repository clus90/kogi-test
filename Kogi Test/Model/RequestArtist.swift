//
//  Request.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import Mantle

class RequestArtist: BaseModel {
    
    var href: String = ""
    var next: String?
    var items: [Artist] = []
    
    override class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["href":"href",
                "next":"next",
                "items":"items"]
    }
    
    class func itemsJSONTransformer () -> NSValueTransformer {
        return MTLJSONAdapter.arrayTransformerWithModelClass(Artist.self)
    }
}


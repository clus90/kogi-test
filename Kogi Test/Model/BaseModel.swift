//
//  BaseModel.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import Mantle

class BaseModel: MTLModel, MTLJSONSerializing {
    
    class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        // override
        return ["":""]
    }
}

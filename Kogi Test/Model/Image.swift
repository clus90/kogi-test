//
//  ArtistImage.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class Image: BaseModel {
    
    var height: CGFloat = 0
    var width: CGFloat = 0
    var url: String = ""
    
    override class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["height":"height",
                "width":"width",
                "url":"url"]
    }

    
}

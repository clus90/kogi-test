//
//  AppManager.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/26/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import MBProgressHUD
import NVActivityIndicatorView

class AppManager: NSObject {
    
    var rootViewController: UIViewController!
    
    static let sharedManager = AppManager()
    
    //This prevents others from using the default '()' initializer for this class.
    private override init() {
        super.init()
        self.rootViewController = ViewFactory.viewControllerForAppView(ViewFactory.AppView.Home)
    }
    
    func showHUD(view: UIView) {
        let indicatorView = NVActivityIndicatorView (frame: CGRectMake(0, 0, 40, 40))
        indicatorView.color = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.AppMainColor)
        indicatorView.type = .LineScaleParty
        indicatorView.startAnimation()
        let loadingNotification = MBProgressHUD.showHUDAddedTo(view, animated: true)
        loadingNotification.customView = indicatorView
        loadingNotification.mode = .CustomView
        loadingNotification.color = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.AppSecundaryColor)
        loadingNotification.dimBackground = true
        loadingNotification.labelColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.AppMainColor)
        loadingNotification.labelText = localizedStringForKey("Loading")
    }
    
    func hideHUD(view: UIView) {
        MBProgressHUD.hideAllHUDsForView(view, animated: true)
    }
}

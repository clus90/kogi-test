//
//  DoubleLabelCell.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class DoubleLabelCell: BaseCell {
    
    var mainLbl: UILabel = UILabel ()
    var secundaryLbl: UILabel = UILabel ()
    
    // Constants
    let kMargin: CGFloat = 20.0
    
    override func setupCell() {
        super.setupCell()
        self.mainLbl.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.DetailMainLabel)
        self.mainLbl.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.DetailMainLabel)
        self.secundaryLbl.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.DetailSecundaryLabel)
        self.secundaryLbl.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.DetailSecundaryLabel)
        self.secundaryLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.contentView.addSubview(self.mainLbl)
        self.contentView.addSubview(self.secundaryLbl)
        
        self.mainLbl.snp_makeConstraints { (make) in
            make.centerY.equalTo(self.contentView)
            make.left.equalTo(self.contentView).offset(kMargin)
        }
        
        self.secundaryLbl.snp_makeConstraints { (make) in
            make.centerY.equalTo(self.contentView)
            make.left.equalTo(self.mainLbl.snp_right).offset(kMargin)
        }
    }
    
    // MARK: - Class functions
    override class func preferredIdentifier() -> String {
        return String(DoubleLabelCell)
    }
    
    override class func preferredHeight() -> CGFloat {
        return 50
    }
}



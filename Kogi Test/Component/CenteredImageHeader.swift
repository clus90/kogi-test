//
//  ArtistImageHeader.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import SnapKit

class CenteredImageHeader: UIView {
    
    var centerImageView: UIImageView = UIImageView ()
    var centerLbl: UILabel = UILabel ()
    
    // Constants
    let kSize: CGFloat = 100.0
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.backgroundColor = UIColor.whiteColor()
        self.addSubview(self.centerLbl)
        self.addSubview(self.centerImageView)
        
        self.centerLbl.text = localizedStringForKey("No_Image")
        self.centerLbl.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.DetailMainLabel)
        self.centerLbl.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.DetailMainLabel)
        
        self.centerImageView.layer.cornerRadius = kSize/2
        self.centerImageView.clipsToBounds = true
        self.centerImageView.contentMode = UIViewContentMode.ScaleAspectFill
        
        self.centerLbl.snp_makeConstraints { (make) in
            make.center.equalTo(self)
        }
        
        self.centerImageView.snp_makeConstraints { (make) in
            make.center.equalTo(self)
            make.size.equalTo(kSize)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 140.0
    }
    
}

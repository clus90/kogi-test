//
//  SearchHeader.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import SnapKit

class SearchHeader: UIView {
    
    var searchTF: UITextField = UITextField ()
    var searchBtn: UIButton = UIButton ()
    
    // Constants
    let kTFLateralMargin: CGFloat = 30.0
    let kTFTopMargin: CGFloat = 40.0
    let kTFHeight: CGFloat = 30.0
    let kSearchBtnHeight: CGFloat = 40.0
    let kSearchBtnWidth: CGFloat = 90.0
    let kButtonTFSeparetion: CGFloat = 20.0
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        self.addSubview(self.searchTF)
        self.addSubview(self.searchBtn)
        
        self.searchTF.placeholder = localizedStringForKey("Search_Artist")
        self.searchTF.backgroundColor = UIColor.whiteColor()
        self.searchTF.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.TextField)
        self.searchTF.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.TextField)
        self.searchTF.clearButtonMode = UITextFieldViewMode.WhileEditing
        
        self.searchTF.snp_makeConstraints { (make) in
            make.top.equalTo(self).offset(kTFTopMargin)
            make.left.equalTo(self).offset(kTFLateralMargin)
            make.right.equalTo(self).offset(-kTFLateralMargin)
            make.height.equalTo(kTFHeight)
        }
        
        let underline: UIView = UIView ()
        underline.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.AppDetailsColor)
        self.addSubview(underline)
        
        underline.snp_makeConstraints { (make) in
            make.top.equalTo(self.searchTF.snp_bottom).offset(2)
            make.left.equalTo(self.searchTF)
            make.right.equalTo(self.searchTF)
            make.height.equalTo(1)
        }
        
        self.searchBtn.backgroundColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.AppSecundaryColor)
        self.searchBtn.setTitle(localizedStringForKey("Search"), forState: UIControlState.Normal)
        self.searchBtn.titleLabel?.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.Button)
        self.searchBtn.titleLabel?.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.Button)
        self.searchBtn.layer.cornerRadius = 5.0
        
        self.searchBtn.snp_makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(self.searchTF.snp_bottom).offset(kButtonTFSeparetion)
            make.width.equalTo(kSearchBtnWidth)
            make.height.equalTo(kSearchBtnHeight)
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 150.0
    }
}

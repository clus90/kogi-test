//
//  AlbumDetailCell.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class AlbumDetailCell: BaseCell {
    
    var albumImageView: UIImageView = UIImageView ()
    var titleLbl: UILabel = UILabel ()
    var marketsLbl: UILabel = UILabel ()
    var marketsListLbl: UILabel = UILabel ()
    
    // Constants
    let kMargin: CGFloat = 20.0
    let kSeparation: CGFloat = 5.0
    let kImageSize: CGFloat = 80
    
    override func setupCell() {
        super.setupCell()
        self.albumImageView.layer.cornerRadius = kImageSize/2
        self.albumImageView.clipsToBounds = true
        self.albumImageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.titleLbl.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.AlbumNameLabel)
        self.titleLbl.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.AlbumNameLabel)
        self.marketsLbl.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.AlbumMarketsLabel)
        self.marketsLbl.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.AlbumMarketsLabel)
        self.marketsLbl.text = localizedStringForKey("Available_Markets")
        self.marketsListLbl.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.AlbumMarketsLabel)
        self.marketsListLbl.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.AlbumMarketsLabel)
        self.contentView.addSubview(self.albumImageView)
        self.contentView.addSubview(self.titleLbl)
        self.contentView.addSubview(self.marketsLbl)
        self.contentView.addSubview(self.marketsListLbl)
        
        self.albumImageView.snp_makeConstraints { (make) in
            make.size.equalTo(kImageSize)
            make.centerY.equalTo(self.contentView)
            make.left.equalTo(self.contentView).offset(kMargin)
        }
        
        self.titleLbl.snp_makeConstraints { (make) in
            make.centerY.equalTo(self.albumImageView).offset(-kMargin)
            make.left.equalTo(self.albumImageView.snp_right).offset(kMargin)
            make.right.equalTo(self.contentView).offset(-kMargin)
        }
        
        self.marketsLbl.snp_makeConstraints { (make) in
            make.left.equalTo(self.titleLbl)
            make.top.equalTo(self.titleLbl.snp_bottom).offset(kSeparation)
            make.right.equalTo(self.contentView).offset(-kMargin)
        }
        
        self.marketsListLbl.snp_makeConstraints { (make) in
            make.left.equalTo(self.titleLbl)
            make.top.equalTo(self.marketsLbl.snp_bottom).offset(kSeparation)
            make.right.equalTo(self.contentView).offset(-kMargin)
        }
    }
    
    // MARK: - Class functions
    override class func preferredIdentifier() -> String {
        return String(AlbumDetailCell)
    }
    
    override class func preferredHeight() -> CGFloat {
        return 120
    }
}

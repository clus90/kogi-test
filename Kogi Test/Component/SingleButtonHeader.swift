//
//  ArtistNameView.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/27/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import SnapKit

class SingleButtonHeader: UIView {
    
    var btn: UIButton = UIButton ()
    
    // Constants
    let kLeftMargin: CGFloat = 20.0
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.backgroundColor = UIColor.whiteColor()
        self.btn.titleLabel?.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.ArtistName)
        self.btn.setTitleColor(AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.ArtistName), forState: UIControlState.Normal)
        
        self.addSubview(self.btn)
        self.btn.snp_makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(kLeftMargin)
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 40.0
    }
    
}

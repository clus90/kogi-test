//
//  GridViewCell.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import AFNetworking

class GridView: BaseCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    var images: [Image] = []
    
    // Constants
    let kSeparation: CGFloat = 10.0
    let kSize: CGFloat = 100.0
    
    override func setupCell() {
        super.setupCell()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        self.collectionView = UICollectionView(frame: self.contentView.frame, collectionViewLayout: layout)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.registerClass(GridCell.self, forCellWithReuseIdentifier: GridCell.preferredIdentifier())
        self.collectionView.backgroundColor = UIColor.whiteColor()
        self.collectionView.scrollsToTop = false
        self.contentView.addSubview(self.collectionView)
        
        self.collectionView.snp_makeConstraints { (make) in
            make.edges.equalTo(self.contentView).inset(UIEdgeInsets(top: 0, left: kSeparation, bottom: 0, right: kSeparation))
        }
    }
    
    func addImagesToGrid (images: [Image]) {
        self.images = images
        self.collectionView.reloadData()
    }
    
    // MARK: - UICollectionView DataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: GridCell = collectionView.dequeueReusableCellWithReuseIdentifier(GridCell.preferredIdentifier(), forIndexPath: indexPath) as! GridCell
        cell.imageView.setImageWithURL(NSURL (string: self.images[indexPath.item].url)!)
        return cell
    }
    
    // MARK: - UICollectionView Delegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(kSize, kSize)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return kSeparation
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return kSeparation
    }
    
    // MARK: - Class functions
    override class func preferredIdentifier() -> String {
        return String(GridView)
    }
    
    override class func preferredHeight() -> CGFloat {
        return 100
    }
}


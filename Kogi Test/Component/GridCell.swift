//
//  GridCell.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/28/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit
import SnapKit

class GridCell: UICollectionViewCell {
    
    let imageView: UIImageView = UIImageView ()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.imageView.clipsToBounds = true
        self.contentView.addSubview(self.imageView)
        self.imageView.snp_makeConstraints { (make) in
            make.edges.equalTo(self.contentView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func preferredIdentifier() -> String {
        return String(GridCell)
    }
    
    class func preferredHeight() -> CGFloat {
        return 100
    }
}

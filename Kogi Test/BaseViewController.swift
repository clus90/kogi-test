//
//  BaseViewController.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/26/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
    private var _tableView:UITableView?
    var tableView:UITableView {
        get {
            if (_tableView == nil) {
                _tableView = UITableView(frame: self.view.frame, style: UITableViewStyle.Plain)
                _tableView!.backgroundColor = UIColor.clearColor()
                _tableView!.separatorStyle = UITableViewCellSeparatorStyle.None
                _tableView!.scrollsToTop = true
                self.view.addSubview(_tableView!)
                self.tableView.snp_makeConstraints(closure: { (make) in
                    make.top.equalTo(self.view.snp_top)
                    make.left.equalTo(self.view.snp_left)
                    make.right.equalTo(self.view.snp_right)
                    make.bottom.equalTo(self.view.snp_bottom)
                })
            }
            
            return _tableView!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupView()
    }
    
    func setupView() {
        self.edgesForExtendedLayout = UIRectEdge.None
        
        if (self.navigationController != nil) {
            self.navigationController!.navigationBar.translucent = false
        }
        self.view.backgroundColor = UIColor.whiteColor()
        
        let endEditingGestureRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.hideKeyboard))
        endEditingGestureRecognizer.numberOfTapsRequired = 1
        endEditingGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(endEditingGestureRecognizer)
        
    }
    
    func hideKeyboard() {
        self.view.endEditing(false)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func pushToView(view:ViewFactory.AppView, completion:((viewController:UIViewController)->())?) {
        let viewController:UIViewController = ViewFactory.viewControllerForAppView(view)
        self.navigationController?.pushViewController(viewController, animated: true)
        
        if (completion != nil) {
            completion!(viewController: viewController)
        }
    }
    
    func presentModalView(view:ViewFactory.AppView, completion:((viewController:UIViewController)->())?) {
        let viewController:UIViewController = ViewFactory.viewControllerForAppView(view)
        self.navigationController?.presentViewController(viewController, animated: true, completion: {
            if (completion != nil) {
                completion!(viewController: viewController)
            }
        })
    }
    
    func showAlertSingleOption (title: String, message: String) {
        let alert = UIAlertController (title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction (title: localizedStringForKey("Accept"), style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
}

//
//  AppTheme.swift
//  Kogi Test
//
//  Created by Carlos Luis Urbina on 8/26/16.
//  Copyright © 2016 Carlos Urbina. All rights reserved.
//

import UIKit

class AppTheme {
    
    enum AppThemeElement: Int {
        case AppMainColor
        case AppSecundaryColor
        case AppDetailsColor
        case TextField
        case Button
        case ArtistName
        case DetailMainLabel
        case DetailSecundaryLabel
        case AlbumNameLabel
        case AlbumMarketsLabel
    }
    
    class func textColorForThemeElement(element:AppThemeElement) -> UIColor{
        
        switch element {
        case .AppMainColor, .Button:
            return UIColor.whiteColor()
        case .AppSecundaryColor, .ArtistName, .DetailMainLabel, .AlbumNameLabel:
            return UIColor (netHex: 0x6ECE3D)
        case .AppDetailsColor, .TextField, .DetailSecundaryLabel, .AlbumMarketsLabel:
            return UIColor (netHex: 0x858585)
        default:
            return UIColor.yellowColor()
        }
    }
    
    class func backgroundColorForThemeElement(element:AppThemeElement) -> UIColor {
        
        switch element {
        case .AppMainColor:
            return UIColor.whiteColor()
        case .AppSecundaryColor:
            return UIColor (netHex: 0x6ECE3D)
        case .AppDetailsColor:
            return UIColor (netHex: 0x858585)
        default:
            return UIColor.yellowColor()
        }
    }
    
    class func fontForThemeElement(element:AppThemeElement) -> UIFont {
        
        switch element {
        case .TextField, .AlbumNameLabel:
            return UIFont.systemFontOfSize(15)
        case .Button:
            return UIFont.boldSystemFontOfSize(15)
        case .ArtistName, .DetailMainLabel:
            return UIFont.boldSystemFontOfSize(17)
        case .DetailSecundaryLabel:
            return UIFont.systemFontOfSize(17)
        case .AlbumMarketsLabel:
            return UIFont.systemFontOfSize(14)
        default:
            return UIFont.systemFontOfSize(10)
        }
    }
}

